(function() {
  'use strict';
  const projectButton = new Kuc.Button({
        text: 'Create Projects',
        type: 'submit',
        id: 'createProject'
  })
  const taskButton = new Kuc.Button({
        text: 'Create Tasks',
        type: 'submit',
        id: 'createTask'
  })
  const button = new Kuc.Button({
      text: 'Duplicate',
      type: 'submit',
      id: 'createContract'
  })
  const text = new Kuc.Text({
    prefix: 'for',
    suffix: 'years　',
    textAlign: 'right',
    id: 'count',
    visible: true,
    disabled: false
  });
  const rate = new Kuc.Text({
    prefix: 'Add',
    suffix: '%　　',
    textAlign: 'right',
    id: 'rating',
    visible: true,
    disabled: false
  });
  kintone.events.on(["app.record.index.show"], event => {
    if ($("#createContract").length > 0) {
      return event;
    }
    $(kintone.app.getHeaderMenuSpaceElement())
      .append(rate)
      .append(text)
      .append(button);
  })
  button.addEventListener('click', function() {
    let client = new KintoneRestAPIClient();
    client.record.getAllRecordsWithCursor({
      app: kintone.app.getId(),
      query:`契約複製 not in ("Yes")`
    })
    .then(createCopyData)
    .then(success => {
      alert("Successed");
      location.reload()
    })
  })
  const createCopyData = (records) => {
    let copyData = []
    let updateRecs = records.map(record => {
      let id = record.$id.value;
      ["contract_id", "作成者", "作成日時", "更新者", "更新日時", "$id"].forEach(deleteField => {
        delete record[deleteField];
      });
      let ratingNumber = (Number(rate.value) + 100) / 100;
      let beforeDate = record.est_date.value;
      let beforeAmount = record.Amount.value
      for (let i = 0; i < text.value; i++){
        let currentRecord = JSON.parse(JSON.stringify(record));
        currentRecord.est_date.value = moment(beforeDate).add(1, "years").format("YYYY-MM-DD")
        currentRecord.Amount.value = Math.floor(beforeAmount * ratingNumber);
        currentRecord.プロジェクト作成.value = []
        if (i + 1 != text.value) {
          currentRecord.契約複製.value = ["Yes"]
        }
        copyData.push(currentRecord)
        beforeAmount = currentRecord.Amount.value;
        beforeDate = currentRecord.est_date.value
      }
      return {
        id: id,
        record: {
          契約複製: {
            value:["Yes"]
          }
        }
      }
    })
    let client = new KintoneRestAPIClient();
    return Promise.all([
      client.record.addAllRecords({
        app: kintone.app.getId(),
        records:copyData
      }),
      client.record.updateAllRecords({
        app: kintone.app.getId(),
        records:updateRecs
      })
    ])
  }
  kintone.events.on(["app.record.create.show", "app.record.edit.show"], event => {
    $(kintone.app.record.getSpaceElement("createTask"))
      .append(taskButton);
    $("#createTask").click(() => { 
      let record = kintone.app.record.get().record;
      let checkedValue = record.project_category.value.map(value => {
        return `"${value}"`
      })
      getTaskList(checkedValue)
      .then(setTaskList)
    })
  })
  const getTaskList = (selectedValue) => {
    return kintone.api("/k/v1/records", "GET", {
      app: 57,
      query:`category in (${selectedValue.join(",")}) order by category asc,step asc,substep asc`
    })
  }
  const setTaskList = (resp) => {
    let getRecord = kintone.app.record.get();
    getRecord["record"].task_table.value = resp.records.map(record => {
      return {
        value: {
          task_id: {
            value: record.task_id.value,
            lookup:true,
            type:"NUMBER"
          },
          task_category: {
            value: record.category.value,
            type:"DROP_DOWN"
          },
          task_name: {
            value: record.task_name.value,
            type:"SINGLE_LINE_TEXT"
          }
        }
      }
    })
    kintone.app.record.set(getRecord)
  }
  kintone.events.on("app.record.detail.show", (event) => {
    if (event.record.プロジェクト作成.value.length > 0) {
      return event;
    }
    $(kintone.app.record.getHeaderMenuSpaceElement()).append(projectButton);
    let contractId = event.recordId;
    let tasks = _.groupBy(
      event.record.task_table.value,
      _.property("value.task_category.value")
    );
    let estimates = _.groupBy(
      event.record["テーブル"].value,
      _.property("value.ドロップダウン.value")
    );

    $("#createProject").click(() => {
      console.log(tasks);
      // Estimateのテーブルのデータをプロジェクトカテゴリごとに分ける
      let projectTables = []
      _.forEach(estimates, est => {
        let fromDate = event.record.est_date.value;
        let projectCategory = est[0].value.ドロップダウン.value
        let cat = projectCategory == "Weekly" ? 52 : projectCategory == "Monthly" ? 12 : projectCategory == "Quarterly" ? 4 : 1;
        let addtionalKey = projectCategory == "Weekly" ? "w" : projectCategory == "Monthly" ? "M" : projectCategory == "Quarterly" ? "Q" : "y"
        let estimateTable = est.map(el => {
          return {
            value: {
              Staff_id: {
                value: el.value.ルックアップ_1.value,
              },
              estimate_hours: {
                value: el.value.estimate_hours.value,
              }
            }
          }
        })
        let  taskTable = []
        if (tasks[projectCategory]) {
          taskTable = tasks[projectCategory].map(el => {
            return {
              value: {
                task_0: {
                  value: el.value.task_id.value,
                },
              }
            }
          })
        }
        let eachProjectTable = []
        for (let i = 0; i < cat; i++) {
          eachProjectTable.push({
            project_category: {
                value: projectCategory,
            },
            // contractId
            ルックアップ: {
              value: contractId,
              // 後ほど "contractId" に変更
            },
            // From (DATE)
            日付: {
              value: fromDate,
            },
            estimate: {
                value: estimateTable
            },
            actual_table: {
              value: taskTable
            },
          })
          //moment.jsが破壊的メソッドのため、一度JSONを再定義する
          let newObj = JSON.parse(JSON.stringify(eachProjectTable));
          let currentDate = newObj[i].日付.value;
          fromDate = moment(currentDate).add(1, addtionalKey).format("YYYY-MM-DD");
        }
        projectTables = projectTables.concat(eachProjectTable)
      })
      let client = new KintoneRestAPIClient();
      client.record.addAllRecords({
        app: 63,
        records: projectTables
      })
      .then(success => {
        kintone.api("/k/v1/record", "PUT", {
          app: kintone.app.getId(),
          id: contractId,
          record: {
            プロジェクト作成: {
              value: ["Yes"]
            }
          }
        }).then(sucs => {
          alert("Successed");
          location.reload()
        })
      })
    });
  });
})();
