(function () {
  "use strict";
  //let now = moment();
  kintone.events.on(
    ["app.record.create.submit", "app.record.edit.submit"],
    function (event) {
      updateTaskInProgressField(event.record);
      return event;
    }
  );

  // kintone.events.on('app.record.index.show', function(event){
  //     //ここの比較で ViewId ではなく View Name を使っているのは
  //     //同じ作りのAppが複数あるので使い回しできるようにです。
  //     if(event.viewName == 'Ongoing Projects' || event.viewName == 'Tasks Assigned To Me'){
  //         DueColourChange(event.records);
  //    }

  // })

  // let DueColourChange = (records) => {

  //     records.forEach((data, index) => {
  //         let projectDue = moment(data['projectDue'].value);
  //         let diff = projectDue.diff(now, 'days');
  //         let projectDueField = kintone.app.getFieldElements('projectDue')[index];
  //         if(diff < 7 && diff > 0){
  //             projectDueField.style.backgroundColor = 'rgba(240, 173, 78, 0.3)';
  //         } else if (diff <= 0){
  //             projectDueField.style.backgroundColor = 'rgba(217, 83, 79, 0.6)';
  //         }
  //     })

  // }

  let updateTaskInProgressField = (record) => {
    let taskTable = record["task_table"]["value"];
    let taskName;
    
    if (record["projectStatus"]["value"] == "Completed") {
      record["taskInProgress"]["value"] = "All Tasks Completed";
    }

    for (let i = 0; i < taskTable.length; i++) {
      taskName = taskTable[i]["value"]["task"]["value"];
      let taskStatus = taskTable[i]["value"]["status"]["value"];
      if (taskStatus != "Completed") {
        record["taskInProgress"]["value"] = taskName;
        return;
      }
    }

   
  };
})();
