(function() {
  'use strict';
  
  kintone.events.on([
    "app.record.create.show", 
    "app.record.create.change.project_category", 
    "app.record.edit.change.project_category",
    "app.record.create.change.task_table", 
    "app.record.edit.change.task_table"], event => {
      let category = event.record.project_category.value == 'Others (Charge)' || event.record.project_category.value == 'Others (Non charge)' ? 'One-off' : event.record.project_category.value;
      event.record.task_table.value.forEach(row=>{
        row.value['task'].value = category;
      })
      return event;
    })

})();
